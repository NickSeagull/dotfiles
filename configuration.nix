# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  _nixpkgs = pkgs;

  pkgsConfig = {
    allowUnfree = true;
  };

in
let

# FIXME: Retry building without this pkgs from GitHub. If it builds properly, remove it
  pkgs = import (
    _nixpkgs.fetchFromGitHub { owner = "NixOS"; repo = "nixpkgs"; rev = "eb2a26f5c61e8cb705bc4a9579502002ebe7ba0c"; sha256 = "19ynv7hzh1f77vc3abp4l1myi8kqavwkc5fd7k52djic7bflf0d3"; }
  ) { config = pkgsConfig; };

in
with pkgs; {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./common.nix
      ./current-profile.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.cleanTmpDir = true;

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?

}
