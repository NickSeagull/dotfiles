# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).


{ config, pkgs, ... }:

rec {
  imports =
    [
      ./users/nick.nix
      ./programs/default.nix
      ./programs/xmonad/default.nix
    ];

    nixpkgs.config.allowUnfree = true;

    networking = {
      hostName = "HighTech-LowLife";
      networkmanager.enable = true;
    };

    i18n = {
      consoleFont = "Lat2-Terminus16";
      consoleKeyMap = "us";
      defaultLocale = "en_US.UTF-8";
    };

    time.timeZone = "Atlantic/Canary";

    programs = {
      gnupg.agent = {
        enable = true;
        enableSSHSupport = true;
      };

      mosh.enable = true;

      zsh = {
        enable = true;
        interactiveShellInit = ''
          export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh
          export TERM=xterm-256color

          # Customize your oh-my-zsh options here
          ZSH_THEME=lambda
          plugins=(git)

          alias t='todo.sh'
          alias e='emacsclient -c'
          alias et='emacsclient'
          alias rebuild-os='sudo nixos-rebuild switch --fallback --show-trace --option binary-caches https://cache.nixos.org/'
          alias stack-docker='stack --docker --docker-stack-exe download'
          alias win10start='sudo virsh start win10 && sudo systemctl stop interception-tools.service'
          alias win10stop='sudo virsh shutdown win10; sudo systemctl start interception-tools.service'

          eval "$(direnv hook zsh)"
          source $ZSH/oh-my-zsh.sh
        '';

        promptInit = "";
      };

      screen = {
        screenrc = ''
          multiuser on
          acladd nick
        '';
      };
    };

    services = {
      printing = {
        enable = true;
        drivers = [ pkgs.hplipWithPlugin ];
      };

      emacs = {
        enable = true;
        defaultEditor = true;
      };

      openssh = {
        enable = true;
        ports = [ 2222 ];
      };

      xserver = {
        enable = true;
        layout = "us";
        xkbOptions = "eurosign:e caps:swapescape";

        serverFlagsSection = ''
          Option "BlankTime" "0"
          Option "StandbyTime" "0"
          Option "SuspendTime" "0"
          Option "OffTime" "0"
        '';

        displayManager.sessionCommands = ''
          xcape -e 'Control_L=Escape'
        '';
      };

      syncthing = {
        enable = true;
        user = "nick";
        dataDir = "/home/nick/.syncthing";
      };

      redshift = {
        enable = true;
        latitude = "28.02958";
        longitude = "-15.41634";
        temperature.night = 2500;
      };

      compton = {
        enable = true;
      };

      weechat = {
        enable = true;
      };

      interception-tools = {
        enable = true;
        plugins = [ pkgs.interception-tools-plugins.caps2esc (pkgs.callPackage ./programs/interception-tools/space2meta.nix {}) ];
        udevmonConfig = ''
          - JOB: "intercept -g $DEVNODE | caps2esc | space2meta | uinput -d $DEVNODE"
            DEVICE:
              EVENTS:
                EV_KEY: [KEY_CAPSLOCK, KEY_ESC, KEY_SPACE]
        '';
      };
    };


    fonts.fonts = with pkgs; [
      inriafonts
      mplus-outline-fonts
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      noto-fonts-extra
      iosevka-bin
      emojione
      emacs-all-the-icons-fonts
    ];

    hardware.pulseaudio.enable = true;

    system.autoUpgrade.enable = true;

    nix = {
      nixPath = [
        (let
          sshConfigFile =
            pkgs.writeText "ssh_config" ''
              Host github.com
              IdentityFile /home/nick/.ssh/id_rsa
              StrictHostKeyChecking=no
            '';
        in
          "ssh-config-file=${sshConfigFile}"
        )
        # The following lines are just the default values of NIX_PATH
        # We have to keep them to not brick the system
        "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos/nixpkgs"
        "nixos-config=/etc/nixos/configuration.nix"
        "/nix/var/nix/profiles/per-user/root/channels"
      ];

      binaryCaches = [ "https://cache.nixos.org/" ];
    };

    sound.enable = true;

}
