# dotfiles

# Usage

1. `git clone git@gitlab.com:NickSeagull/dotfiles.git ~/.dotfiles`
2. `sudo mv /etc/nixos/hardware-configuration.nix ~/.dotfiles`
3. `sudo rm -r /etc/nixos/`
4. `sudo ln -s ~/.dotfiles /etc/nixos`
5. `sudo nixos-rebuild switch`

Enjoy!

## Credits

This repository is heavily inspired by nmattia's [Homies](https://github.com/nmattia/homies), and
Geoffrey Huntley [dotfiles](https://github.com/ghuntley/dotfiles-nixos/) please check them out!
