{ config, pkgs, ... }:

let

  config = import ./config.nix { inherit pkgs; };

in
{
  virtualisation = {
    libvirtd = {
      enable = true;
      qemuOvmf = true;
      qemuVerbatimConfig = config;
    };

    docker = {
      enable = true;
      enableOnBoot = true;
      autoPrune.enable = true;
    };
  };
}
