{ config, lib, pkgs, ... }:
with lib;

{
  users.users.nick = {
    description = "Nick Tchayka";
    isNormalUser = true;
    home = "/home/nick";
    extraGroups = [
      "audio"
      "input"
      "docker"
      "kvm"
      "libvirtd"
      "networkmanager"
      "users"
      "vboxusers"
      "video"
      "weechat"
      "wheel"
    ];
    uid = 1000;
    shell = "/run/current-system/sw/bin/zsh";
  };
}
