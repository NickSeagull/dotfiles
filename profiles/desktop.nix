{ config, pkgs, ... }:

{
  imports =
    [ ../virtualization ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "vfio_virqfd" "vfio_pci" "vfio_iommu_type1" "vfio" ];
    extraModprobeConfig = "options vfio-pci ids=10de:1402,10de:0fba";
    postBootCommands = ''
      DEVS="0000:05:00.0 0000:05:00.1"

      for DEV in $DEVS; do
        echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
      done
      modprobe -i vfio-pci
    '';
    kernelParams = [ "amd_iommu=on" ];
  };
  services.xserver.videoDrivers = ["nvidia"];

  # Disable tearing
  services.xserver.screenSection = ''
    Option "metamodes" "nvidia-auto-select +0+0 { ForceCompositionPipeline = On }"
  '';
}
