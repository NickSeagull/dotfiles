{ config, pkgs, ... }:

{
  virtualisation.docker = {
      enable = true;
      enableOnBoot = true;
      autoPrune.enable = true;
  };
  services.xserver.libinput.enable = true;
  services.xserver.videoDrivers = ["intel"];
  services.tlp.enable = true;

  # hardware.pulseaudio.package = pkgs.pulseaudioFull;
  # hardware.pulseaudio.extraModules = [ pkgs.pulseaudio-modules-bt ];
  # hardware.bluetooth.enable = true;
  # hardware.bluetooth.extraConfig = "
  #   [General]
  #   Enable=Source,Sink,Media,Socket
  # ";
  # hardware.pulseaudio.configFile = pkgs.writeText "default.pa" ''
  #   load-module module-bluetooth-policy
  #   load-module module-bluetooth-discover
  #   ## module fails to load with 
  #   ##   module-bluez5-device.c: Failed to get device path from module arguments
  #   ##   module.c: Failed to load module "module-bluez5-device" (argument: ""): initialization failed.
  #   ## load-module module-bluez5-device
  #   load-module module-bluez5-discover
  # '';
}
