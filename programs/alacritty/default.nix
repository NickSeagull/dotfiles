{ symlinkJoin, makeWrapper, writeText, alacritty }:
let
  configFile = writeText "alacritty.yml" (import ./alacritty.nix {writeText = writeText;});
in
  symlinkJoin {
    name = "alacritty";
    buildInputs = [makeWrapper];
    postBuild = ''
      wrapProgram "$out/bin/alacritty" \
          --add-flags "--config-file ${configFile}" \
    '';
    paths = [ alacritty ];
  }
