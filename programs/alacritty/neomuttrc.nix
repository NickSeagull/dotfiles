{ writeText, mailcapPath }:
let
  workFile = writeText "workacc" (import ./work.nix);
  personalFile = writeText "personalacc" (import ./personal.nix);
  draculaTheme = writeText "draculatheme" (import ./dracula.nix);
in
''
set mailcap_path = ${mailcapPath}
# Decrypt passwords
source "gpg -dq $HOME/.my-pwds.gpg |"

set header_cache = "~/.mutt/cache/headers"
set message_cachedir = "~/.mutt/cache/bodies"
set record = ""
set imap_check_subscribed
set imap_keepalive = 300
unset imap_passive
set mail_check = 60

set signature = "echo '--------------------------------------------\nQ: Why is this email five sentences or less?\nA: http://five.sentenc.es\n\nSent using Neomutt, Neovim and NixOS'|"

set editor = "nvim +/^$/+1 +':noh'"

set sidebar_visible = yes


## ACCOUNT1
source "${workFile}"
# Here we use the $folder variable that has just been set in the sourced file.
# We must set it right now otherwise the 'folder' variable will change in the next sourced file.
folder-hook $folder 'source ${workFile}'

## ACCOUNT2
source "${personalFile}"
folder-hook nikitatchayka@gmail.com 'source ${personalFile}'

## Shortcuts
macro index,pager <f2> '<sync-mailbox><enter-command>source ${personalFile}<enter><change-folder>!<enter>'
macro index,pager <f3> '<sync-mailbox><enter-command>source ${workFile}<enter><change-folder>!<enter>'


## THEME
source "${draculaTheme}"

########################################################
#                                                      #
#                  M A C R O S                         #
#                                                      #
########################################################
## Make quick html view macro
bind index,pager V  noop        ## Unbinds V from version
macro index,pager V "<view-attachments><search>html<enter><view-mailcap><exit><enter>"


## Fix changing folders in multiple accounts
macro index 'c' '<change-folder>?<change-dir><home>^K=<enter>'
''
