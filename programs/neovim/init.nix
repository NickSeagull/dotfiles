''
" ================================================================"
"====                                                        ===="
"====             GENERAL EDITOR SETTINGS
"====                                                        ===="
"================================================================"
set showmode
set smartcase
set smarttab
set smartindent
set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2
set laststatus=0
"let g:deoplete#enable_at_startup = 1
if !exists('g:deoplete#omni#input_patterns')
  let g:deoplete#omni#input_patterns = {}
endif
" let g:deoplete#disable_auto_complete = 1
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
set termguicolors
call deoplete#custom#option('sources', {
    \ 'purescript': ['psc-ide'],
\})
let g:SuperTabDefaultCompletionType = "<c-x><c-u>"
color dracula
autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE
set background=dark
let g:haskell_tabular = 1
"================================================================"
"====                                                        ===="
"====             GENERAL KEYBOARD REBINDINGS                ===="
"====                                                        ===="
"================================================================"

" We make Space act as the leader key, like in Spacemacs
nnoremap <SPACE> <Nop>
let mapleader = " "

" Make ESC work in neovim terminals
tnoremap <Esc> <C-\><C-n>

" Bind jk to ESC
imap jk <Esc>

" SPC b d - Close buffer
nnoremap <leader>bd :bd<CR>

" SPC f s - Save file
nnoremap <leader>fs :w<CR>

" SPC p f - Find project
nnoremap <leader>pf :CtrlP<CR>

" SPC p t - Project tree
nnoremap <leader>pt :NERDTreeToggle<CR>

" SPC w <hjkl> - Move around panels
nnoremap <leader>wh <C-\><C-n><C-w>h
nnoremap <leader>wj <C-\><C-n><C-w>j
nnoremap <leader>wk <C-\><C-n><C-w>k
nnoremap <leader>wl <C-\><C-n><C-w>l

" SPC w v - Split verical
nnoremap <leader>wv <C-w>v

" SPC w s - Split horizontal
nnoremap <leader>ws <C-w>s

" SPC w d - Close window
nnoremap <leader>wd <C-w>c

" SPC q q - Close all
nnoremap <leader>qq :qall

" SPC g s - Git status
nnoremap <leader>gs :Gstatus<CR>

" SPC g p - Git push
nnoremap <leader>gp :Gpush<CR>

" SPC g C - Git commit
nnoremap <leader>gC :Gcommit<CR>

" SPC a = - Align on =
nnoremap <leader>a= :Tabularize /=<CR>

" SPC a ; - Align on ::
nnoremap <leader>a; :Tabularize /::<CR>

" SPC a - - Align on ->
nnoremap <leader>a- :Tabularize /-><CR>

"================================================================"
"====                                                        ===="
"====             FUZZY SEARCHING (CTRL-P)
"====                                                        ===="
"================================================================"
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'


"================================================================"
"====                                                        ===="
"====             LANGUAGE SERVER CONFIGURATION              ===="
"====                                                        ===="
"================================================================"
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
    \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
    \ 'python': ['/usr/local/bin/pyls'],
    \ 'haskell': ['hie', '--lsp'],
    \ }
let g:LanguageClient_rootMarkers = ['*.cabal', 'stack.yaml']

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
map <Leader>lk :call LanguageClient#textDocument_hover()<CR>
map <Leader>lg :call LanguageClient#textDocument_definition()<CR>
map <Leader>lr :call LanguageClient#textDocument_rename()<CR>
map <Leader>lf :call LanguageClient#textDocument_formatting()<CR>
map <Leader>lb :call LanguageClient#textDocument_references()<CR>
map <Leader>la :call LanguageClient#textDocument_codeAction()<CR>
map <Leader>ls :call LanguageClient#textDocument_documentSymbol()<CR>

hi link ALEError Error
hi Warning term=underline cterm=underline ctermfg=Yellow gui=undercurl guisp=Gold
hi link ALEWarning Warning
hi link ALEInfo SpellCap

"================================================================"
"====                                                        ===="
"====             HASKELL SYNTAX HIGHLIGHTING CONFIG         ===="
"====                                                        ===="
"================================================================"
let g:haskell_classic_highlighting = 0
let g:haskell_indent_if = 3
let g:haskell_indent_case = 2
let g:haskell_indent_let = 4
let g:haskell_indent_where = 6
let g:haskell_indent_before_where = 2
let g:haskell_indent_after_bare_where = 2
let g:haskell_indent_do = 3
let g:haskell_indent_in = 1
let g:haskell_indent_guard = 2
let g:haskell_indent_case_alternative = 1
let g:cabal_indent_section = 2
"================================================================"
"====                                                        ===="
"====             PURESCRIPT CONFIGURATION FOR VIM           ===="
"====                                                        ===="
"================================================================"
let g:psc_ide_syntastic_mode = 1
"================================================================"
"====                                                        ===="
"====                 MARKDOWN TABLE MODE                    ===="
"====                                                        ===="
"================================================================"
function! s:isAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', \'\', \'\'), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

let g:table_mode_corner='|'

inoreabbrev <expr> <bar><bar>
          \ <SID>isAtStartOfLine('\|\|') ?
          \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
          \ <SID>isAtStartOfLine('__') ?
          \ '<c-o>:silent! TableModeDisable<cr>' : '__'
''
