{ symlinkJoin, fetchFromGitHub, makeWrapper, neovim, cargo, rustc, vimUtils, vimPlugins }:
neovim.override {
  viAlias = true;
  vimAlias = true;
  configure = {
    customRC = import ./init.nix;
    vam.pluginDictionaries = [
      { names = [ "fzf-vim" ]; }
      { names = [ "deoplete-nvim" ]; }
      { names = [ "vim-pandoc" ]; }
      { names = [ "ctrlp" ]; }
      { names = [ "fugitive" ]; }
      { names = [ "gitgutter" ]; }
      { names = [ "nerdtree" ]; }
      { names = [ "surround" ]; }
      { names = [ "dhall-vim" ]; }
      { names = [ "haskell-vim" ]; }
      { names = [ "vim-nix" ]; }
      { names = [ "vim-pandoc-syntax" ]; }
      { names = [ "vim-table-mode" ]; }
      { names = [ "LanguageClient-neovim" ]; }
      { names = [ "purescript-vim" ]; }
      { names = [ "psc-ide-vim" ]; }
      { names = [ "syntastic" ]; }
      { names = [ "vim-colors-solarized" ]; }
      { names = [ "supertab" ]; }
      { names = [ "tabular" ]; }
      { names = [ "vim-stylish-haskell" ]; }
      { names = [ "direnv-vim" ]; }
      { names = [ "ale" ]; }
      { names = [ "vim" ]; } # This is the dracula vim theme lol
    ];
  };
}
