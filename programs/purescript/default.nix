{ config, pkgs, ... }:

let

  easyPS = import (pkgs.fetchFromGitHub {
    owner = "justinwoo";
    repo = "easy-purescript-nix";
    rev = "a9be7dcde4785063d9f8dab2be646d6e9905ab66";
    sha256 = "1vg2fax48kbgy15qlwd41iavqmjwyqsyw5m97mlscck1iiqzidf2";
  });

in
{
  inherit(easyPS.inputs)
   purs
   psc-package-simple
   purp
   spago
   spacchetti-cli
   ;
}
