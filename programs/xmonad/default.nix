{ config, pkgs, ... }:

let

  i3Config = import ./config.nix { inherit pkgs; };

in
{
  services.xserver.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
  };

  services.xserver.displayManager.lightdm = {
    enable = true;
  };

  services.xserver.desktopManager = {
    default = "none";
    xterm.enable = false;
  }; 
}
