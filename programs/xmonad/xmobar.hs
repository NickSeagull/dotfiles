Config
{ font = "xft:Iosevka-10"
, bgColor = "black"
, fgColor = "grey"
, position = TopW L 100
, commands =
  [ Run Cpu
    [ "-L", "3"
    , "-H", "50"
    , "--normal", "green"
    , "--high","red"
    ] 10
  , Run Network "wlp3s0" ["-L","0","-H","32","--normal","green","--high","red"] 10
  , Run Memory
    [ "-t","Mem: <usedratio>%"
    ] 10
  , Run Date "%d %b %Y - %k:%M" "date" 10
  , Run Battery
    [ "-t", "<acstatus>: <left>% - <timeleft>"
    , "--"
    , "-O", "AC"
    , "-o", "Bat"
    , "-h", "green"
    , "-l", "red"
    ] 10
  , Run Com "bash" ["-c", "~/.dotfiles/programs/xmonad/scripts/get-volume.sh" ]  "myvolume" 1
  , Run Com "bash" ["-c", "echo `xbacklight -get | grep -oE '^.[0-9]{0,3}'`%"]  "mybright" 1
  , Run StdinReader
  ]
, sepChar = "%"
, alignSep = "}{"
, template = "%StdinReader% }{ %battery% | Vol: %myvolume% | Lum: %mybright% | <fc=#e6744c>%date%</fc>"
}
