import           XMonad
import           XMonad.Actions.Navigation2D
import           XMonad.Actions.CycleWS
import           XMonad.Config
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Layout.BinarySpacePartition
import           XMonad.Layout.FixedColumn
import           XMonad.Layout.Grid
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.NoBorders
import           XMonad.Layout.Spacing
import           XMonad.Layout.Fullscreen
import           XMonad.Layout.Spiral
import           XMonad.Layout.Tabbed
import           XMonad.Layout.ThreeColumns
import qualified XMonad.StackSet as W
import qualified Graphics.X11.ExtraTypes.XF86 as XF86

import           XMonad.Util.CustomKeys
import           XMonad.Util.Run

import           Data.List

backgroundColor = "#202020"
middleColor = "#AEAEAE"
foregroundColor = "#AAFFAA"

main = do
  xmobarPipe <- spawnPipe xmobarCommand
  xmonad def
    { modMask = mod4Mask
    , terminal = "alacritty"
    , logHook = dynamicLogWithPP $ myXmobarPP xmobarPipe
    , handleEventHook = docksEventHook
    , layoutHook = smartBorders . avoidStruts . spacingWithEdge 4 $ myLayout
    , manageHook = myManageHooks
    , borderWidth = 1
    , focusedBorderColor = foregroundColor
    , focusFollowsMouse = False
    , keys = myKeys
    }

-- Color of current window title in xmobar.
xmobarTitleColor = "#FFB6B0"

-- Color of current workspace in xmobar.
xmobarCurrentWorkspaceColor = "#CEFFAC"

myLayout =
    Tall 1 (3/100) (1/2)
    ||| Mirror (Tall 1 (3/100) (1/2))

myManageHooks = composeAll
  [ manageDocks
  , isFullscreen --> doFullFloat
  , isDialog --> doCenterFloat
  , className =? "looking-glass-client" --> doFullFloat
  , className =? "stalonetray" --> doFullFloat
  , className =? "pavucontrol" --> doFullFloat
  , className =? "kazam" --> doFullFloat
  , manageHook defaultConfig
  ]

myXmobarPP xmobarPipe = def
  { ppCurrent = xmobarColor xmobarCurrentWorkspaceColor "" . pad
  , ppHidden = pad
  , ppHiddenNoWindows = pad
  , ppLayout = const ""
  , ppOutput = hPutStrLn xmobarPipe
  , ppTitle = xmobarColor xmobarTitleColor "" . shorten 100 . pad
  , ppVisible = pad
  , ppWsSep = " "
  }

xmobarCommand :: String
xmobarCommand = intercalate " "
  [ "xmobar"
  , "-d"
  , "-B", quoted "#202020"
  , "-F", quoted "#AEAEAE"
  ]
 where
  quoted x = "\"" ++ x ++ "\""


frames =
  map show [-35, -30 .. 0]

toBashArray = intercalate ", "

openTray = unlines
  [ "stalonetray --window-type normal --background '#282a36'"
  , "xdotool windowmove $TRAY_ACTIVE 0 -35"
  , "for i in "<> toBashArray frames <>";do xdotool windowmove $TRAY_ACTIVE 0 $i; sleep 0.015; done"
  ]

closeTray = unlines
  [ "for i in "<> (toBashArray $ reverse frames) <>";do xdotool windowmove $TRAY_ACTIVE 0 $i; sleep 0.015; done"
  , "pkill stalonetray"
  ]

toggleTray = unlines
  [ "export TRAY_ACTIVE=$(xdotool search 'stalonetray' | head -1)"
  , "if [[ -z \"${TRAY_ACTIVE}\" ]]; then " <> openTray <> "; else "<> closeTray <>"; fi"
  ]

myKeys = customKeys removedKeys addedKeys
removedKeys XConfig {modMask = modm} = []
addedKeys conf@XConfig {modMask = modm}   =
  -- Run rofi
  [ ((modm, xK_Delete), spawn "rofi -show run")

  -- Org capture from rofi
  , ((modm, xK_b), spawn "rofi  -show fb -modi fb:~/.dotfiles/programs/rofi/capture.sh")

  -- Hoogle search
  -- , ((modm, xK_u), spawn "rofi  -show fb -modi fb:~/.dotfiles/programs/rofi/hoogle.sh")

  -- Open GTD inbox
  , ((modm, xK_g), spawn "emacsclient -c ~/Sync/org/inbox.org")

  -- Open coding projects
  , ((modm, xK_e), spawn "emacsclient -c ~/Code")

  -- Toggle tray
  , ((modm, xK_n), spawn toggleTray)

  -- Run terminal
  , ((modm, xK_t), spawn $ XMonad.terminal conf)

  -- Cycle through the available layout algorithms.
  , ((modm, xK_space),
     sendMessage NextLayout)

  --  Reset the layouts on the current workspace to default.
  , ((modm .|. shiftMask, xK_space),
     setLayout $ XMonad.layoutHook conf)

  -- Shrink the master area.
  , ((modm, xK_u),
     sendMessage Shrink)

  -- Expand the master area.
  , ((modm, xK_i),
     sendMessage Expand)

  -- Push window back into tiling.
  , ((modm .|. shiftMask, xK_t),
     withFocused $ windows . W.sink)

  -- Directional window navigation
  , ((modm, xK_l), windowGo R False)
  , ((modm, xK_h), windowGo L False)
  , ((modm, xK_k), windowGo U False)
  , ((modm, xK_j), windowGo D False)

  -- Go to next screen
  , ((mod4Mask, xK_o), nextScreen)

  -- Swap screens
  , ((mod4Mask .|. mod1Mask, xK_o), swapNextScreen)
  , ((0, XF86.xF86XK_MonBrightnessDown), spawn "xbacklight -10")
  , ((0, XF86.xF86XK_MonBrightnessUp), spawn "xbacklight +10")
  , ((0, XF86.xF86XK_AudioRaiseVolume),  spawn "pactl set-sink-volume @DEFAULT_SINK@ +10%")
  , ((0, XF86.xF86XK_AudioLowerVolume),  spawn "pactl set-sink-volume @DEFAULT_SINK@ -10%")
  , ((0, XF86.xF86XK_AudioMute),         spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
  ]

