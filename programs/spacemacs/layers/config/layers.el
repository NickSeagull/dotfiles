(configuration-layer/declare-layers
 '(;; Core
   (auto-completion :variables
                    auto-completion-return-key-behavior 'complete
                    auto-completion-tab-key-behavior 'complete
                    auto-completion-enable-snippets-in-popup t)
   git
   helm
   org
   (shell :variables
          shell-default-shell 'eshell)
   syntax-checking
   spell-checking
   (version-control :variables
                    version-control-global-margin t
                    version-control-diff-tool 'git-gutter+)
   lsp

   ;; Misc
   ranger

   ;; Markups
   csv
   html
   markdown
   yaml

   ;; Languages
   emacs-lisp
   (haskell :variables haskell-process-type 'cabal-new-repl)
   nixos
   javascript
   ruby
))
