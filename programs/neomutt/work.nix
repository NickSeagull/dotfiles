''
set imap_user = "nikita@theagilemonkeys.com"
set imap_pass = $my_work_pass
set smtp_url = "smtp://nikita@theagilemonkeys@smtp.gmail.com:587"
set smtp_pass = $my_work_pass
set folder = "imaps://imap.gmail.com:993"
set spoolfile = "+INBOX"
set postponed = "+[GMail]/Drafts"

set realname = "Nikita Tchayka"

## Hook -- IMPORTANT!
account-hook $folder "set imap_user=nikita@theagilemonkeys.com imap_pass=$my_work_pass"
''
