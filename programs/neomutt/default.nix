{ symlinkJoin, makeWrapper, writeText, neomutt }:
let
  mailcapPath = writeText "mailcap" (import ./mailcap.nix);
  configFile = writeText "neomuttrc" (import ./neomuttrc.nix {writeText = writeText; mailcapPath = mailcapPath; });
in
  symlinkJoin {
    name = "neomutt";
    buildInputs = [makeWrapper];
    postBuild = ''
      wrapProgram "$out/bin/neomutt" \
          --add-flags "-F ${configFile}" \
    '';
    paths = [ neomutt ];
  }
