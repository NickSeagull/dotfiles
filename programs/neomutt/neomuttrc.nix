{ writeText, mailcapPath }:
let
  workFile = writeText "workacc" (import ./work.nix);
  personalFile = writeText "personalacc" (import ./personal.nix);
  draculaTheme = writeText "draculatheme" (import ./dracula.nix);
in
''
set mailcap_path = ${mailcapPath}
# Decrypt passwords
source "gpg -dq $HOME/.my-pwds.gpg |"

set header_cache = "~/.mutt/cache/headers"
set message_cachedir = "~/.mutt/cache/bodies"
set record = ""
set imap_check_subscribed
set imap_keepalive = 300
unset imap_passive
set mail_check = 60
set signature = "echo '--------------------------------------------\nQ: Why is this email five sentences or less?\nA: http://five.sentenc.es\n\nSent using Neomutt, Neovim and NixOS'|"
set editor = "nvim +/^$/+1 +':noh'"
set sidebar_visible = yes
set sort="threads"
set strict_threads="yes"
set sort_browser="reverse-date"
set sort_aux="last-date-received"
unset collapse_unread


## ACCOUNT1
source "${workFile}"
folder-hook $folder 'source ${workFile}'

## ACCOUNT2
source "${personalFile}"
folder-hook nikitatchayka@gmail.com 'source ${personalFile}'

## THEME
source "${draculaTheme}"

########################################################
#                                                      #
#                  M A C R O S                         #
#                                                      #
########################################################

## Make quick html view macro
bind index,pager V  noop        ## Unbinds V from version
macro index,pager V "<view-attachments><search>html<enter><view-mailcap><exit><enter>"

## Fix changing folders in multiple accounts
macro index 'c' '<change-folder>?<change-dir><home>^K=<enter>'

## Open personal mail
macro index,pager <f2> '<sync-mailbox><enter-command>source ${personalFile}<enter><change-folder>!<enter>'

## Open work mail
macro index,pager <f3> '<sync-mailbox><enter-command>source ${workFile}<enter><change-folder>!<enter>'

# Threads
bind index - collapse-thread
bind index _ collapse-all

## Gmail-style keyboard shortcuts
bind index g noop
macro index ga "<change-folder>=[Gmail]/Todos<enter>" "Go to all mail"
macro index gd "<change-folder>=[Gmail]/Borradores<enter>" "Go to drafts"
macro index gi "<change-folder>=INBOX<enter>" "Go to inbox"
macro index gs "<change-folder>=[Gmail]/Destacados<enter>" "Go to starred messages"
macro index gt "<change-folder>=[Gmail]/Papelera<enter>" "Go to trash"
macro index,pager d "<save-message>=[Gmail]/Papelera<enter><enter>" "Trash"
macro index,pager y "<save-message>=[Gmail]/Todos<enter><enter>" "Archive"
''
