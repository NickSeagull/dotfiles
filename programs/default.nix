{ config, pkgs, stdenv, callPackage, ...}:
let
  my = {
    alacritty = pkgs.callPackage ./alacritty {};
    discord = pkgs.callPackage ./discord {};
    hies = pkgs.callPackage ./haskell-ide-engine {};
    looking-glass-client = pkgs.callPackage ./looking-glass-client {};
    neomutt = pkgs.callPackage ./neomutt {};
    neovim = pkgs.callPackage ./neovim {};
    ps = pkgs.callPackage ./purescript {};
  };
in
{
  environment.systemPackages = with pkgs; [
    ag
    appimage-run
    arandr
    awscli
    cabal2nix
    catdocx
    chromium
    dunst
    direnv
    escrotum
    ffmpeg
    file
    firefox
    gcc
    ghostscript
    gimp
    git
    gnumake
    gnupg
    gparted
    htop
    httpie
    imagemagick
    interception-tools
    ispell
    jq
    kazam
    keepassxc
    keybase
    libreoffice
    links2
    mplayer
    obs-studio
    oh-my-zsh
    pamixer
    pandoc
    pavucontrol
    pciutils   # lspci
    poppler_utils # merge pdfs and stuff
    rofi
    rtorrent
    saneBackends
    slack
    smartmontools
    spotify
    stalonetray
    syncthing
    syncthing-cli
    syncthing-gtk
    tmux
    todo-txt-cli
    torbrowser
    tree
    unzip
    viewnior
    vifm
    virtmanager
    w3m
    weechat
    wget
    xautomation
    xdotool
    zathura
    zip

    haskellPackages.xmobar
    haskellPackages.apply-refact
    haskellPackages.hasktags
    haskellPackages.hlint
    haskellPackages.hoogle
    haskellPackages.stylish-haskell
    haskellPackages.ghc

    xcape
    xclip
    xorg.xrdb
    xorg.setxkbmap
    xorg.xset
    xorg.xsetroot
    xorg.xinput
    xorg.xprop
    xorg.xauth
    xorg.xmodmap
    xorg.xbacklight

    # Custom packages
    my.alacritty
    my.discord
    my.neomutt
    my.neovim
    my.looking-glass-client
    my.ps.psc-package-simple
    my.ps.purp
    my.ps.purs
    my.ps.spacchetti-cli
    my.ps.spago
  ];
}
