#!/usr/bin/env bash

if [ x"$@" = x"Cancel" ]
then
    exit 0
fi

if [ "$@" ]
then
    ALL_DATA=$(curl https://hoogle.haskell.org/\?mode\=json\&hoogle\=map\&start\=1 | jq '.[] | {url: .url, module: .module.name, definition: .item}')
else
    echo -en "\x00prompt\x1fHoogle\n"
    echo "Cancel"
fi
