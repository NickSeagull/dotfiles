#!/usr/bin/env bash

if [ x"$@" = x"Cancel" ]
then
    exit 0
fi

if [ "$@" ]
then
    # Override the previously set prompt.
    echo "* TODO $@" >> ~/Sync/org/inbox.org
    exit 0
else
    echo -en "\x00prompt\x1fCapture\n"
    echo -en "\0message\x1fPress <b>RET</b> to save\n"
    echo "Cancel"
fi
